
# Manage a Developer Archive Library of Binary Images for Embedded Development Using NPM

## But Why?

This project has a single value added proposition: 

**Replace the use of SCM as a 'Private, Long Term Reference Library of Firmware Images for Access By Internal Developers and CI' with NPM."**

Please note:
- "Private" - there is nothing about this approach that suggests the pushing of binary images to the public NPM registry.
- "Internal Developers" - while some may be quick to envision using this for external distribution as well - this project is not suggesting solving distribution with NPM. (only because the problems created for internal Git SCM storing binaries are substantial and require more immediate focus)

Many firmware and other industries have used their legacy Source Code Management systems to store the built binary images of their software. This has been most prevalent in at least the Gaming and Embedded Development industry verticals. 

When such organizations want to modernize their SCM to to Git, this becomes an antipattern because Git does not diff binaries and so the history bloats with many copies of the binary. While Git-LFS helps with the repository impact, it still does not help with full history clones of the repository as Git-LFS objects are still pulled.

While SCM has historically functioned to add important capabilities to long term historic storage including version control, in the time since this practice emerged, there have been many advances in binary artifact packaging technology that make it much more suitable.

So what if we could get all the sought after benefits of SCM and more, by moving this storage to a well known, existing package manager?

**Precedents**: Repurposing a developer dependency manager for this kind of storage has been done in the past. The success story of Chocolatey NuGet (yum for Windows) was also a tale of reusing most of a capable dependency manager to manage operating system installers.

### Requirements That SCM Meets

1. Firmware images that go to production need to be kept indefinitely.
2. They need to be easily findable.
3. Their release date needs to be known.
4. Their content needs to be immutable (new varitions always have unique identity) and/or signable.

### NPM confers additional advantages (while retaining SCM advantages):

1. Solves a boring "Wheel Problem" with boring technology. As is evidenced by the additional bullets below, the problem of versioned, immutable, attestable, binary artifact storage with easy retrieval has been well solved. Reusing a proven, workhorse technology is quick, efficient and already debugged.
2. Built-in [provenance attestation](https://docs.gitlab.com/ee/ci/yaml/signing_examples.html#verifying-npm-provenance) that with Sigstore can span organizations. `[BUILT-IN TO EXAMPLE]`
3. Hosting by any NPM hosting service.

### Migrations Out of SCM

1. Ability to force created and modified dates in NPM metadata to match generation dates for binaries when migrating them out of SCM and into a package manager.
2. Can run NPM packaging in migration automation.

#### Storage Management and Cost

1. Removes the file bloat from Git.
1. File compression for stored artifacts. `[BUILT-IN TO EXAMPLE]`
1. Ability to remove binaries easily - for example preproduction runs (binaries in Git are nearly impossible to remove even if unneeded). `[BUILT-IN TO EXAMPLE]`
1. Easy retention and clean up automation of non-production packages (registry feature). `[BUILT-IN TO EXAMPLE]`
1. The ability to back the storage with highly durable, intelligent tiering cloud object storage. Git file systems (other than LFS), must be backed with high performance, streaming storage.

#### Availability and Familiarity

1. Standardized private distribution repositories. `[BUILT-IN TO EXAMPLE]`
1. The NPM client is available and easily installable for all operating systems and even minimal environments like containers.
1. Easily accessible versioned history from CLIs and Web UI catalogs.
1. Built in "latest" released package tracking. `[BUILT-IN TO EXAMPLE]`
1. Can still easily use the packaged binary images in build process where this is a need by using npm install. `[BUILT-IN TO EXAMPLE]`
1. Enforce version immutability - the same version cannot be republished (configurable). `[BUILT-IN TO EXAMPLE]`
1. Dependency management between binary image files when groupings are relevant. (if relevant).
1. Package meta data in registry. `[BUILT-IN TO EXAMPLE]`
1. Package access statistics (in NPM).

### Software Supply Chain Security & Provenance for Functional Safety Requirements
1. Signing [GitLab supports Sigstore signing of NPM](https://docs.gitlab.com/ee/ci/yaml/signing_examples.html#use-sigstore-and-npm-to-generate-keyless-provenance). This also allows software supply chain security to carry distinct signing from actualy binary signing if necessary. `[BUILT-IN TO EXAMPLE]`
1. Attestation and Release (when used with GitLab Release support). `[BUILT-IN TO EXAMPLE]`
1. A signed package that contains only dependency relationships can be used to attest provenance for collections of firmware released together for a complex system of embedded systems like automotive..
1. Can be paired with GitLab Releases for further attestation. `[BUILT-IN TO EXAMPLE]`

### Concerns:

1. **Max Package Size**: Public NPM registry largest package is 6 GB (as of April 2024)

### ARCHITECTURE

This project simply uses standard user installed NPM packages. To stay safe from messing up anything about an machine with node, it simply includes a postinstall script that copies the files to a directory in /tmp

User is responsible for cleaning up /tmp and any npm packages that end up in their 

Shell scripts will be developed that push the user to /tmp before even doing the initial npm user install so that packages are not even installed into an existing user install.

### Working Examples

- [[GitLab CI Embedded Firmware Build]](https://gitlab.com/explore/projects/topics/GitLab+CI+Embedded+Firmware+Build) - projects that use GitLab CI to do actual firmware builds (not just store source).
- [[Firmware Library]](https://gitlab.com/explore/projects/topics/Firmware+Library) - projects that use NPM to create a company internal developers firmware archive instead of storing firmware binaries in Git.
- [[Firmware with Package Management]](https://gitlab.com/explore/projects/topics/Firmware+With+Package+Management) - projects that use NPM as a firmware binaries package manager.
### Thinking Out Loud

The huge list of advantages is available by using this pattern only for Developers accessing a long term firmware image archive.

It's not intended for customer distribution nor application of firmware. GitLab Releases can function as a customer distribution system.

It is possible to extend the functionality to do the following, but this project does not delve into these areas:
- distribute firmware images to downstream developers who don't mind installing npm to interact with the package registry
- add more utility scripts to do more interesting things with the files.


